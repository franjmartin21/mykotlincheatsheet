package com.fran.kotlincheatsheet.section1Basics

/**
 * COLLECTIONS
 *
 * Instantiation of different type of collections
 */
fun main(args: Array<String>) {
    //List<T> //It is immutable

    //MutableList<T> //Mutable list

    val array = arrayOf(2,3,4,5,6,67) //This creates an array

    println(array) //It will print as type [Ljava.lang.Integer;@6f94fa3e

    val array2 = intArrayOf(2,34,234,2,53,4536,6)

    val list = listOf(32,43,45,6,34,6,36,45,7,7)//To create an immutable list...
    println(list)

    //list[0] = 99 this will give a compilation error

    val list2 = mutableListOf(2,4,5,6,7,8,5,4)

    list2[0] = 99 //this works!


    //SAME FOR SET
    val set = setOf(1,2,2,3,4,5)
    println(set) //removes the repeticion

    //there is a mutable set too

    //MAPS
    val map = mapOf(Pair(1, "Kotlin"), Pair(2,"Android"))
    println(map)
    //Same for mutable map
    val mutableMap = mutableMapOf(1 to "Kotlin", 2 to "Android")//we can use the "to" word
}