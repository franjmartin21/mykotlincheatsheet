package com.fran.kotlincheatsheet.section1Basics

/**
 * CONDITIONALS, IF EXPRESSIONS
 *
 * Different usages of the if clauses to control the flow of the program in Kotlin
 */
fun main(args: Array<String>) {
    val i = 17
    /* All this below is the same than Java */
    if(i > 15){
        println("i is pretty small")
    }
    if(i < 15){
        println("i is pretty small")
    } else{
        println("it's pretty large")
    }

    if(i < 15){
        println("i is pretty small")
    }
    else if(i >= 15 && i <= 25) {
        println("it's okay")
    } else{
        println("it's pretty large")
    }

    // In kotlin you can assign an expression to a variable
    val x = if(i < 15){
        println("i is pretty small")
        "small" //the last line of each block is actually like the return value of it
    }
    else if(i >= 15 && i <= 25) {
        println("it's okay")
        "medium"
    } else{
        println("it's pretty large")
        "large"
    }

    println(x)
}