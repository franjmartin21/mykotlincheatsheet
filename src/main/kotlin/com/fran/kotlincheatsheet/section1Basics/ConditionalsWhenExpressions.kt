package com.fran.kotlincheatsheet.section1Basics

/**
 * CONDITIONALS, WHEN EXPRESSIONS - AKA switch or case expressions
 *
 * Different usages of the when clauses to control the flow of the program in Kotlin
 */
fun main(args: Array<String>) {
    val price = 29
    when(price){
        //We don't need either case or brake in kotlin
        0 -> println("For free today")
        in 1..19 -> println("On sale") // you can give ranges by using ..
        in 20..29 -> println("Normal price")
        else -> println("Overpriced") //else for default
    }

    when(price){
        0 -> println("For free today")
        !in 1..19 -> println("Not on Sale")// We can use negative expressions for ranges too
        else -> println("Overpriced")
    }

    //Yet another style of sintax
    when{
        price <= 19 -> println("Sale")
        price >= 19 -> println("Normal price")
        else -> println("Overpriced")
    }

    // And as before with if we can assign to a variable
    //Yet another style of sintax
    val x = when{
        price <= 19 -> println("Sale")
        price >= 19 -> println("Normal price")
        else -> println("Overpriced")
    }

}