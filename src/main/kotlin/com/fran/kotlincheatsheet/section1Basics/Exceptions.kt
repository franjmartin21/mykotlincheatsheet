package com.fran.kotlincheatsheet.section1Basics

import java.io.IOException

/**
 * EXCEPTIONS
 */
fun main(args: Array<String>) {

    /*
     * Unlike Java we don't need to initialize the variable input first and then inform it in the try sentence
     * we can just assign to input the result of the expression
     */
    val input = try{
        getExternalInput()
    } catch (e: IOException){
        e.printStackTrace()
        null
    } finally {
        println("Finished trying to read external input")
    }
    println(input)

    //TODO: Add more information about exceptions
}

fun getExternalInput(): String {
    throw IOException("Could not read external input")
}