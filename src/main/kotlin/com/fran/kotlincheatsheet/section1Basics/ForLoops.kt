package com.fran.kotlincheatsheet.section1Basics

/**
 * FOR LOOPS
 *
 * This file shows different ways of doing for loops
 */
fun main(args: Array<String>) {

    for(i in 1..10){
        println("$i")
    }

    for (c in "Kotlin"){
        println("$c")
    }

    var languages = listOf("Kotlin", "java", "Swift")
    languages.filter { it.contains("t") }.forEach {
        println(it)
    }

    for(lang in languages){
        println("$lang")
    }

    for(i in 10 downTo 1){
        println("$i")
    }

    for(i in 10 downTo 1 step 2){
        println("$i")
    }
}