package com.fran.kotlincheatsheet.section1Basics

/**
 *
 */
fun main() {
    println(permitEntrance(7))
    println(permitEntrance(87))

    println(permitEntrance2(7))
    println(permitEntrance2(87))

    println(permitEntrance3(4,78))
}

fun permitEntrance(age: Int): Boolean{
    return age >= 18
}


fun permitEntrance2(age: Int): Boolean = age >= 8


fun permitEntrance3(vararg ages: Int): Boolean{
    return ages.any{age -> age >= 18}
}

