package com.fran.kotlincheatsheet.section1Basics

/**
 * This feature of kotlin of declaring optional parameters replaces the old methodology in java of declaring overloaded
 * methods
 */
fun main(args: Array<String>) {
    // This line calls the method just with one param. As "separator" param it will be used the default value
    val togetherDefault = concat(listOf("Java", "Kotlin", "Android"))
    println(togetherDefault)

    //This line calls the method with the separator argument
    val together = concat(listOf("Java", "Kotlin", "Android"), separator = ":")
    //by naming explicitely the arguments, you can change the order if you want
    println(together)
}

/**
 * This method contains one mandatory parameter and one optional parameter
 * args:
 *  - texts: It is mandatory because it does not have default value
 *  - separator: optional parameter, because it contains the default value for the param
 *
 */
fun concat(texts: List<String>, separator: String =", ") = texts.joinToString(separator)
