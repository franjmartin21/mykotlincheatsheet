package com.fran.kotlincheatsheet.section1Basics

fun main(args: Array<String>) {
    //By default variables are not nullable (null safety)
    //val str: String = null //not possible to do

    val str: String? = null // This ? makes the variable nullable

    //str.length Kotlin detects the problem on calling the property of a null object at compile-time

    str!!.length //The !! will remove the compile error and it will throw the null pointer...

    str?.length //The ? makes the expression safe, if str is null it will just return null when calling any method or property in the str val

    //Another example of safe expression
    val str2: String? = "Peter"

    str2?.length

    // ELVIS OPERATOR
    // Kotlin also makes easy to assign a default value when the property is null. ie
    val strLength = str?.length ?: 0 // so if the expression is null then it will actually return 0


}