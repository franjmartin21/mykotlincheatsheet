package com.fran.kotlincheatsheet.section1Basics

/**
 * When declaring a variable as val. You are making it immutable. You cannot change the value of it
 */
fun main(args: Array<String>) {
    // Using the repl in this lesson

    //2 ways to create variables
    var age = 24

    //Kotlin can infere the type but it is strongly typed

    // you can also do
    var age2: Int = 24

    // This is an immutable variable
    val name = "Peter"

    // I cannot do
    //name = "Peter 2"

    //Different types of variables
    val isDeveloper: Boolean = true

    //Float, Char, Double

    //Always prefer val over var when possible

    //You can use of type inference

}
