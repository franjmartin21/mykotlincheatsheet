package com.fran.kotlincheatsheet.section1Basics

/**
 * This class just shows the classical old stype while and do-while loop in kotlin
 */
fun main(args: Array<String>) {
    var i = 1
    while(i <= 10){
        println("$i")
        i++
    }

    do{
        println(i)
        i++
    } while (i<= 10)



}