package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun main(args: Array<String>) {

    /*
     * This creates a list of maps
     * Each map is composed from one String as key and one list as vale
     */
    val inputRows = listOf(
            mapOf("input01.csv" to listOf(3, 5, -9977, 7 , 11, 66)),
            mapOf("input02.csv" to listOf(1, 3, 4)),
            mapOf("input03.csv" to listOf()),
            mapOf("input04.csv" to listOf(9989, 33, 14, 12, 5))
    )
    /*
     * This chain of functions does:
     *
     * 1. It creates a flat map with the values of each map in the list
     * result: [3, 5, -9977, 7, 11, 66], [1, 3, 4], [], [9989, 33, 14, 12, 5]
     *
     * 2. flatten makes all the values part of the same list
     * result: 3, 5, -9977, 7, 11, 66, 1, 3, 4, 9989, 33, 14, 12, 5
     *
     * 3. filter { it in 0..100 } // it will filter just the values between 0 and 100
     * result: 3, 5, 7, 11, 66, 1, 3, 4, 33, 14, 12, 5
     *
     * 4. sorted() sorts list
     * result: 1, 3, 3, 4, 5, 5, 7, 11, 12, 14, 33, 66
     */
    val cleaned = inputRows.flatMap { it.values }
            .flatten()
            .filter { it in 0..100 }
            .sorted()
            .toIntArray()

    println(cleaned.joinToString())
}