package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun modifyString(str: String, operation: (String) -> String): String {
    return operation(str)
}
// We can make the previous function an "inline" function
inline fun modifyString2(str: String, operation: (String) -> String): String {
    return operation(str)
}


fun main(args: Array<String>) {
    val myString = "Hello Kotlin"
    modifyString(myString, {it.toUpperCase()})
}