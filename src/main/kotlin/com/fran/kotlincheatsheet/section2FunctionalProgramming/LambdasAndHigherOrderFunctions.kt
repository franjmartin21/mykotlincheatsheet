package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun main(args: Array<String>) {

    //Lambda is like a function without a name
    {x: Int -> x*2} // So this would be a lambda, where it is specified the param and the operation on the param

    //that can be stored in a variable.
    val timesTwo: (Int) -> Int =  {x: Int -> x*2}
    //Kotlin can infere the type so...
    val timesTwo2 = {x: Int -> x*2}

    // Other example of lambda...
    val add: (Int, Int) -> Int = {x: Int, y: Int -> x+y} // So type is defined as two param int function to int return
    //Again, you can just do...
    val add2 = {x: Int,y: Int -> x+y}

    /* Below it is showed how a lambda can be used in function */
    val list = (1..100).toList()
    println(list.filter({element -> element % 2 == 0}))

    /* When there is only one paramter for the lambda, we can make this even more concise as kotlin provides the param it by default so...*/
    println(list.filter({it % 2 == 1}))

    /* Finally we can augment funcionality of Int by using an extension function and use it in our lambda */
    println(list.filter({it.even()}))

    /* Or we can just call another function */
    println(list.filter({isEven(it)}))
    /* Or call it in a even more concise way by calling the function with the :: notation */
    println(list.filter(::isEven))

    /* Finally, we can move the lambda off the parentesis*/
    list.filter {it % 2 == 0}
    list.filter {isEven(it)}
}

fun Int.even() = this % 2 == 0

fun isEven(i: Int) = i % 2 == 0