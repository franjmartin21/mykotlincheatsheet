package com.fran.kotlincheatsheet.section2FunctionalProgramming

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val veryLongList = (1..999999L).toList()

    val sum = veryLongList
            .filter { it > 50 }
            .map { it * 2 }
            .take(1000)
            .sum()

    // The order is important, in this case, take cannot go before filter as that would change the result
    // However we could and should do take before map as that would speed significantly the operation

    println(sum)

    //Let's do it using lazy evaluation now with sequence
    val sum2 = veryLongList
            .asSequence()
            .filter { it > 50 }
            .map { it * 2 }
            .take(1000)
            .sum()

    println(sum2)

    //Creating a sequence
    val seq = generateSequence(1) { it + 1 }

    println(seq.take(10).toList())

    // PERFORMANCE IN LAZY SEQUENCES

}