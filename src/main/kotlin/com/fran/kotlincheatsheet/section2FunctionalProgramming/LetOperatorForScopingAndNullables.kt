package com.fran.kotlincheatsheet.section2FunctionalProgramming

import java.io.File

fun main(args: Array<String>) {

    //function called let()
    // This is an example using SCOPING. The scope of the buffered reader is limited to the function passed as param
    File("example.txt").bufferedReader().let{ reader -> //We could also use the it sintax
        if(reader.ready()){
            println(reader.readLine())
        }
    }

    // --> Reader would not be visible here

    //Working with NULLABLES
    val str: String? = "Kotlin for Android"

    //No so nice as we need to use the !! notation that makes the expression unsafe to NullPointerException
    if(str!!.isNotEmpty()){
        str.toLowerCase()
    }

    //Using let
    str?.let {
        if(it.isNotEmpty()){
            it.toLowerCase()
        }
    }

}