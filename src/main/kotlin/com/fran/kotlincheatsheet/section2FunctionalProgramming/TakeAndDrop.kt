package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun main(args: Array<String>) {

    //val list = (1..1000).toList()
    val sequence = generateSequence(0){it + 10} //It generates a sequence starting by the seed and applying the lambda to each next element

    //sequence is lazy evaluated when doing toList

    val first10 = sequence.take(10).toList() //Takes the first 10 elements
    //val withoutFirst900 = list.drop(900).toList() //Drops the first 900 elements

    //you also have other methods like first or last

    println(first10)
    //println(withoutFirst900)


}