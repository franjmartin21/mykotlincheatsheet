package com.fran.kotlincheatsheet.section2FunctionalProgramming

import java.io.FileReader

fun main(args: Array<String>) {
    //.use, can be used by every object that implements the closeable interface
    FileReader("mayexists.txt").use{
        val str = it.readText()
        println(str)
        //use will always call the .close() method of the object
    }
}