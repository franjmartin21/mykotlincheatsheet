package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun main(args: Array<String>) {

    // USING MAP
    val list = (1..100).toList()

    val doubled = list.map { element -> element * 2 }

    println(doubled)

    val double2 = list.map { it * 2 }

    println(double2)

    val average = list.average()
    val shifted = list.map { it - average }

    println(average)
    println(shifted)

    // USING FLATMAP
    // first creating a list of lists
    val nestedList = listOf(
            (1..10).toList(),
            (11..20).toList(),
            (21..30).toList(),
            (31..40).toList(),
            (41..50).toList(),
            (51..60).toList(),
            (61..70).toList())

    val notFlattened = nestedList.map{it.sortedDescending()}
    // This sorts each list in isolation, and when printing it will print every list in desending order
    println(notFlattened)

    val flattened = nestedList.flatMap { it.sortedDescending() }
    //flatMap sorts the list and then unifies all lists as part of just one list
    // it would have been the same to do
    //nestedList.map{it.sortedDescending()}.flatten()
    println(flattened)
}