package com.fran.kotlincheatsheet.section2FunctionalProgramming

fun main(args: Array<String>) {
    val props = System.getProperties()

    with(props){
        list(System.out)
        println(propertyNames().toList())
        println(getProperty("user.home"))
    }
    //By doing this you avoid to use the name of the variable all the time that you call its methods
    //The previous would be equivalent with doing
    props.propertyNames().toList()
    props.getProperty("user.home")
}