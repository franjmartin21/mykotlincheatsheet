package com.fran.kotlincheatsheet.section3ObjectOrientation

abstract open class Shape2(val name: String){//Abstract means that it cannot be instantiated it, just for inheritance

    abstract fun area(): Double //Will need to be overriden
}

class Circle2(name: String, private val radius: Double): Shape2(name){
    //In this case we are forced to implement the method defined in the super class
    override fun area() = Math.PI * Math.pow(radius, 2.0)
}


fun main(args: Array<String>) {
    val smallCircle = Circle2("Small Circle",2.0)
    println(smallCircle.name)
    println(smallCircle.area())
}