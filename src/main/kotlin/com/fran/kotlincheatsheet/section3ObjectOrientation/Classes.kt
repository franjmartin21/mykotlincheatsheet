package com.fran.kotlincheatsheet.section3ObjectOrientation

class Empty
// we don't need {} if we don't want to provide an actual body


fun main(args: Array<String>) {
    val empty = Empty()
}