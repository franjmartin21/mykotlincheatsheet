package com.fran.kotlincheatsheet.section3ObjectOrientation

//Companion object it is like the equivalent of Static in java
class House(val numberOfRooms: Int, val price: Double){


    companion object {
        val HOUSES_FOR_SALE = 10
        fun getNormalHouse() = House(6, 199_999.0)
        fun getLuxuryHouse() = House(42, 7_000_000.0)
    }

    //name is optional but we can also give a name to the companion object
    /*
    companion object Factory{
        val HOUSES_FOR_SALE = 10
        fun getNormalHouse() = House(6, 199_999.0)
        fun getLuxuryHouse() = House(42, 7_000_000.0)
    }
    */
}

fun main(args: Array<String>) {
    //Then the methods can be called like a static method in java
    println(House.getLuxuryHouse())
}



//one distiction with Static in java is that you can actually implement interfaces in a companion object
//EXAMPLE
interface HouseFactory{
    fun createHouse(): House2
}

class House2(val numberOfRooms: Int, val price: Double){

    companion object: HouseFactory {
        fun getNormalHouse() = House2(6, 199_999.0)
        fun getLuxuryHouse() = House2(42, 7_000_000.0)
        override fun createHouse():House2 =  House2.getNormalHouse()

    }
}