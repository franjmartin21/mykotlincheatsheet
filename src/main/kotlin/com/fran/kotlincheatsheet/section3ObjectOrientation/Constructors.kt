package com.fran.kotlincheatsheet.section3ObjectOrientation

/**
class Country(name: String, areaSqKm: Int){
    /*
    val name: String
    val area: Int

    //The compiler will complain as the vals has not been initialized, one solution is
    //provide an init piece...
    init{
        this.name = name
        this.area = areaSqKm
    }
     */

    //In Kotlin we don't need the init block we just do
    val name: String= name
    val area: Int = areaSqKm

}
*/

/* We can even go one step forward by declaring the vals directly in the arguments*/
class Country(val name: String, var area: Int){
    //So then we can just use them in our methods

    fun print() = "$name, $area km2"

    //We can do secundary constructors like below, you always need to delegate to the main constructor
    constructor(name: String): this(name, 500_000){
        //Extra logic...
    }


}

fun main(args: Array<String>) {
    println(Country("Spain", 500_000).print())
    println(Country("Spain"))
}

