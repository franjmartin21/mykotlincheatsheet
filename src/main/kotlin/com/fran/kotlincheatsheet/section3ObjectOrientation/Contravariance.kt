package com.fran.kotlincheatsheet.section3ObjectOrientation

//This kind of classes are called producers as they just return values T but never consume, this allow us to use them with
//more flexibility like showed in the main method
//class Source<T>(val t: T){
class Source<out T>(val t: T){
    fun produceT(): T{
        return t
    }
}

fun main(args: Array<String>) {
    val strSource: Source<String> = Source("Producer")
    val anySource: Source<Any> = strSource //This just works if it is marked as out T

}

// Follow example in contravariance 2



