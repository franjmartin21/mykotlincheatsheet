package com.fran.kotlincheatsheet.section3ObjectOrientation


//Now, lets assume that the class also have functions that consume the generic T
/*class Source2<T>(val t: T){
    fun consumeT(t: T){

    }

    fun produceT(): T{
        return t
    }
}*/

//Now, we are going to create what it is called a contravariant class
class Sink<in T>{
    fun consumeT(t: T){

    }
    //fun a(): T {} // I cannot create a method that returns T if the class is defined <in T>
}

fun main(args: Array<String>) {
    val strSource: Source<String> = Source("Producer")
    val anySource: Source<Any> = strSource //This just works if it is marked as out T
    // This was an example of out -> Covariance

    anySource.produceT()

    //Now we will use the consumer class
    val anySink: Sink<Any> = Sink()
    val strSink: Sink<String> = anySink //This is possible because we have marked it as <in T>
    strSink.consumeT("")
    // This was an example of in -> Contravariance

    //Invariance -> neither covariant nor contravariant
}