package com.fran.kotlincheatsheet.section3ObjectOrientation

open class Being
open class Person:Being()
open class Student:Person()

fun main(args: Array<String>) {
    val people: MutableList<Person> = mutableListOf(Person(), Person())
    people.add(Student())//covariance

    //people.add(Being()) //You cannot do that as being is a less specific type than Person

    val p: Any = Person()

    val students: List<Person> = listOf<Student>()

    //val students2: MutableList<Person> = mutableListOf<Student>() //this is not allowed

}