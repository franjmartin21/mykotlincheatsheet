package com.fran.kotlincheatsheet.section3ObjectOrientation

//data class generates hashCode, equals, toString, copy, destructuring...
data class Address(val street: String, val number: Int, val postCode: Int, val city: String)

fun main(args: Array<String>) {
    val residence = Address("Malecon Balta", 192, 23423, "Lima")
    val residence2 = Address("Malecon Balta", 192, 23423, "Lima")

    println(residence)
    //This returns true if Address is a "data" class but false if it is not. data class contains the equal method
    println(residence2 == residence)
    //This is the equivalent to == in java, it checks if the reference is the same, so it is always false in this case
    println(residence2 === residence)

    //copy() as mentioned "data" class will contain a copy method
    val residence3 = residence.copy()
    //copy accepts parameters for the attribute you want to change

    //Destructuring() operator allows to do the following
    val(street, number, postCode, city) = residence
    println("$street $number $postCode $city")
    //and also
    val st = residence.component1()
    println(st)
}