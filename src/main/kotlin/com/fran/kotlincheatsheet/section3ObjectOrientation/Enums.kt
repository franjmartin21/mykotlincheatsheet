package com.fran.kotlincheatsheet.section3ObjectOrientation


enum class Directions(val degree: Double){
    NORTH(0.0), EAST(90.0), SOUTH(180.0), WEST(270.0)
}

enum class Suits{
    HEARTS, SPADES, DIAMONDS, CLUBS
}

fun main(args: Array<String>) {
    val dir = Directions.NORTH
    println(dir.degree)

    val suit = Suits.SPADES

    val color = when(suit){
        Suits.SPADES, Suits.DIAMONDS -> "Red"
        Suits.CLUBS, Suits.HEARTS -> "Black"
    }

    println(color)
}