package com.fran.kotlincheatsheet.section3ObjectOrientation

//EXTENSION FUNCTIONS are a useful way to get rid of util static methods that we used to use in java
//This would be an example of extension function that extends Int
fun Int.isEven(): Boolean = (this % 2 == 0)


fun main(args: Array<String>) {
    //We can use the function then
    println(5.isEven())

    //Or even used it as a lambda expression
    val naturals = (11..100).toList().filter { it.isEven() }
    println(naturals)

}