package com.fran.kotlincheatsheet.section3ObjectOrientation

import java.util.*

/*
fun main(args: Array<String>) {
    val list: List<Int> = listOf(1,2,3)
    println(list)

}
*/
//Creating a generic class
class Timeline<E> {

    val data2data:MutableMap<Date, E> = mutableMapOf()

    fun add(element: E){
        data2data.put(Date(), element)
    }

    fun getLatests(): E{
        return data2data.values.last()
    }
}

//If we want to create a method that returns something of the same type than the parameter you can do
fun<E> timelineOf(vararg elements: E):Timeline<E>{
    val result = Timeline<E>()
    for(element in elements){
        result.add(element)
    }
    return result
}

fun main(args: Array<String>) {
    val timeline: Timeline<Int> = Timeline()
    timeline.add(2)
    println(timeline.getLatests())
}
