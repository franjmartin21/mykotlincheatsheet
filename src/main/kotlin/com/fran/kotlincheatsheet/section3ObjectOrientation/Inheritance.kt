package com.fran.kotlincheatsheet.section3ObjectOrientation

//Minimal example
open class Base //Open makes the class inheritable, the default in Kotlin is close(final in java)

class Child: Base()

open class Shape(val name: String){

    open fun area() = 0.0 //open means that can be overridden
}

class Circle(name: String, private val radius: Double): Shape(name){
    override fun area() = Math.PI * Math.pow(radius, 2.0)
}


fun main(args: Array<String>) {
    val smallCircle = Circle("Small Circle",2.0)
    println(smallCircle.name)
    println(smallCircle.area())
}