package com.fran.kotlincheatsheet.section3ObjectOrientation


//We define an interface...
interface Drivable {
    //val a:Int//Interfaces in Kotlin can have properties, and force to implement in the subclases
    fun drive()//You can also provide a default implementation.... but is not recommended by the author of the course
}


class Bicycle: Drivable{
    override fun drive() {
        println("Driving Bicycle")
    }
}

class Boat: Drivable{
    override fun drive() {
        println("Driving boat")
    }
}


fun main(args: Array<String>) {
    val drivable: Drivable = Bicycle()//Using polimorfism...
    drivable.drive()
}