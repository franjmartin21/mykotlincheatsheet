package com.fran.kotlincheatsheet.section3ObjectOrientation

class Robot(val name: String){

    fun greetHuman(){
        println("Hello Human. My name is $name")
    }
    fun knowsItsName():Boolean = name.isNotEmpty()

}

fun main(args: Array<String>) {
    val fightRobot = Robot("R2D2")
    if(fightRobot.knowsItsName())
        fightRobot.greetHuman()
}