package com.fran.kotlincheatsheet.section3ObjectOrientation

import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent

//object is like building a singleton in Java
object CountryFactory{ //You cannot have constructor in this case as it does not make sense constructor if you just can have one of this
    val a = 4
    fun createCountry(): Country = Country("Australia")
}


fun main(args: Array<String>) {
    //val obj = CountryFactory()// This does not work to create an "object"
    CountryFactory.createCountry() //So again, this is similar to static classes in java in its notation
}

// object are a good replacement for anonimous classes in java for instance you can do...
object DefaultClickListener: MouseAdapter(){
    override fun mouseClicked(e: MouseEvent?) {
        println("Mouse was clicked")
    }
}