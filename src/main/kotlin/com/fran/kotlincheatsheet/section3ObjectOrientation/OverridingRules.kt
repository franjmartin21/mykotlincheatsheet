package com.fran.kotlincheatsheet.section3ObjectOrientation

// Kotlin checks at compile time to check if you are using overriding, if you are overriding a method in a super class
// or a method of the interface that you are implementing, kotlin forces you to use the keyword "override"

abstract class Vehicle{
    open fun drive(){
        println("Driving")
    }
    abstract fun honk()
}

class Sedan: Vehicle(), Drivable{
    override fun drive() {
        //super.drive()
        /**
         * The author illustrates in this example what would happen if the interface Drivable provided a default implementation
         * of the method drive. It would not be clear which method exactly the super.drive() would be calling, therefore kotlin
         * will show a compilation error
         */
        super<Vehicle>.drive()//Allows to explicitely call the super method of Vehicle
        //super<Drivable>.drive()//Allows to call the super method of Drivable
    }

    override fun honk() {
        println("Mooop")
    }
}

fun main(args: Array<String>) {
    val sedan = Sedan()
    sedan.drive()
    sedan.honk()
}

/**
 * RULE 2: You can override a member which is val with var, but not the other way arround
 *
 * RULE 3: You need to implement all the abstract methods of super classes in your concrete java class
 * you may not implement an abstract method, if your class is marked as abstract too
 */