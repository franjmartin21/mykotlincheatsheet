package com.fran.kotlincheatsheet.section3ObjectOrientation

class City{
    //var name: String = ""
    //If we want null we would need to do...
    //var name: String? = null

    /* If we would like to rewrite getters and setters...*/
    var name: String = ""
        get() = field
        set(value) {
            field = value
        }
    // These two previous implementation are actually the same than the default one


    var population: Int = 0
    //We use var as we want these properties to be mutable, we would not be able
    //to set a proper value later on, otherwise


}

fun main(args: Array<String>) {
    val berlin = City()
    //Lines below work because Kotlin creates getters and setters automagically

    //So this would implicitelly call setters...
    berlin.name = "Berlin"
    berlin.population = 3_500_000

    //And this would implicitelly call getters...
    println(berlin.name)
    println(berlin.population)
}