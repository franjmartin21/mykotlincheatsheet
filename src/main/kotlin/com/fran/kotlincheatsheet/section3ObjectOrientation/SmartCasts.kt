package com.fran.kotlincheatsheet.section3ObjectOrientation


//we province a couple of extension methods that we will use in our example
fun Bicycle.replaceWheel(){
    println("Replacing wheel")
}
fun Boat.startEngine():Boolean{
    println("Starting engine")
    return true
}

fun main(args: Array<String>) {
    val vehicle: Drivable = Bicycle()

    vehicle.drive()//Vehicle just have the method drive
    //instanceof <-> is in kotlin
    if(vehicle is Bicycle){
        //in java it would be now...
        //val b = ((Bicycle)vehicle)
        //in KOTLIN cast works automatically once you made the "is" comparison
        //so we can just call the bicycle method
        vehicle.replaceWheel()
    } else if(vehicle is Boat){
        vehicle.startEngine()
    }

    //We can even place it in the same if
    if(vehicle is Boat && vehicle.startEngine()){//in the second part of the engine, the vehicle is already a Boat

    }

    //You can also do...
    if(vehicle !is Boat || vehicle.startEngine()){

    }

    //Finally you can even do
    if(vehicle !is Bicycle){
        return
    }
    //so at this point the compiler knows that the object is Bicycle for sure...
    vehicle.replaceWheel()

}