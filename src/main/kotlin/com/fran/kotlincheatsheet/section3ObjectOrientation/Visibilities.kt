package com.fran.kotlincheatsheet.section3ObjectOrientation

// private - Same than in Java
// protected - Same than in Java
// no package visibility
// internal (not existing in java), visible inside the same module. Module for kotlin is a set of kotlin files compiled together
////if only one module, then internal is like public
// public - Same than in Java

class Car(val brand: String, private val model: String) {//public default visibility for both the class and the members

    fun tellMeYourModel() = model

}


fun main(args: Array<String>) {
    val car = Car("BRAND", "MODEL")
    car.brand
    //car.model//error as it is defined as private
    car.tellMeYourModel()//protected, or private would make it invisible here
}